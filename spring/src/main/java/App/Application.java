package App;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import App.Repositories.Admin.*;
import App.Models.Admin.*;

@SpringBootApplication
public class Application implements CommandLineRunner {

    @Autowired
    JobTitleRepository jobTitleRepository;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {
        JobTitle model1 = new JobTitle();
        model1.setJobTitle("Pres");
        jobTitleRepository.save(model1);
        JobTitle model2 = new JobTitle();
        model2.setJobTitle("Sec");
        jobTitleRepository.save(model2);
        System.out.println("FIND ALL: ");
        for (JobTitle jobtitle : jobTitleRepository.findAll()) {
            System.out.println(jobtitle);
        }
    }

}
