
package App.Models.Admin;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class JobTitle {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    private String job_title;
    private String job_description;

    public JobTitle() {}

    public void setJobTitle(String job_title) {
        this.job_title = job_title;
    }
    public void setJobDescription(String job_description) {
        this.job_description = job_description;
    }
    public String toJSON() {
        return String.format("{" + 
            "\"job_title\":\"%s\"," + 
            "\"job_description\":\"%s\"," + 
            "\"id\":%d}",
            job_title,job_description,id);
    }
}