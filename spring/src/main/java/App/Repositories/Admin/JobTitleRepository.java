package App.Repositories.Admin;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

import App.Models.Admin.JobTitle;

public interface JobTitleRepository extends CrudRepository<JobTitle, Long> {

}
