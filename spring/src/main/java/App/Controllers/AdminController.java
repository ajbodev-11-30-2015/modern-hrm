package App.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class AdminController {

    @RequestMapping(value="/admin/{entity}", method=RequestMethod.GET)
    public String admin(@PathVariable String entity) {
        System.out.println(entity);
        return "admin/" + entity;
    }

}
