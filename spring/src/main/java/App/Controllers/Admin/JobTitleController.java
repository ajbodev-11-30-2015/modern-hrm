
package App.Controllers.Admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.StringJoiner;
import App.Repositories.Admin.JobTitleRepository;
import App.Models.Admin.JobTitle;

@Controller
@RequestMapping("/jobtitle")
public class JobTitleController {

    @Autowired
    JobTitleRepository repository;

    @RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
    public @ResponseBody String getEdit(@PathVariable long id) {
        JobTitle entity = repository.findOne(id);
        return entity.toJSON();
    }

    @RequestMapping(value="/list", method=RequestMethod.GET)
    public @ResponseBody String getList() {
        StringJoiner entities = new StringJoiner(",");
        for (JobTitle entity : repository.findAll()) {
            entities.add(entity.toJSON());
        }
        return "[" + entities.toString() + "]";
    }

    @RequestMapping(value="/add", method=RequestMethod.POST)
    public @ResponseBody String postAdd(String job_title, String job_description) {
        JobTitle entity = new JobTitle();
        entity.setJobTitle(job_title);
        entity.setJobDescription(job_description);
        repository.save(entity);
        return "Added";
    }

    @RequestMapping(value="/update", method=RequestMethod.POST)
    public @ResponseBody String postUpdate(long id, String job_title, String job_description) {
        JobTitle entity = repository.findOne(id);
        entity.setJobTitle(job_title);
        entity.setJobDescription(job_description);
        repository.save(entity);
        return "Updated " + id;
    }

    @RequestMapping(value="/delete", method=RequestMethod.POST)
    public @ResponseBody String postDelete(long id) {
        repository.delete(id);
        return "Deleted " + id;
    }

}