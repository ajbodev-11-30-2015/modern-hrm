RubyRor::Application.routes.draw do
  
  get 'admin/:entity', to: 'adminctrl#entity' 
  
  match 'app/time', to: 'app/time#index', via: :all
    
  get ':controller/:action(/:id)', constraints: { action: /list|edit/ }  
  post ':controller/:action', constraints: { action: /add|update|delete/ }
    
  # Admin
  match 'jobtitle/:action(/:id)', to: 'admin/jobtitle#:action', via: :all
end