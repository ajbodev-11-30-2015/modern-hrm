
class Admin::JobtitleController < ApplicationController
  def list
    entities = Admin::JobTitle.all
    render json: entities
  end
  def edit
    entity = Admin::JobTitle.find_by(id: params[:id])
    render json: entity
  end
  def add
    entity = Admin::JobTitle.create(
      job_title: params[:job_title],
    )
    render :text => 'Added'
  end
  def update
    entity = Admin::JobTitle.find_by(id: params[:id])
    entity.job_title = params[:job_title]
    entity.save
    render :text => 'Updated'
  end
  def delete
    entity = Admin::JobTitle.find_by(id: params[:id])
    entity.destroy
    render :text => 'Deleted'
  end
end