class AdminctrlController < ApplicationController
  def entity
    render file: 'admin/' + params[:entity] + '.html'
  end
end
