<?php 

namespace App\Entity\Admin;

use Illuminate\Database\Eloquent\Model;

class JobTitle extends Model {

	protected $connection  = 'orangehrm';
	protected $table       = 'ohrm_job_title';
	public    $timestamps  = false;

}
