<?php

Route::get('/', function () {
    return view('welcome');
});

// ModernHRM

Route::controllers([
    'admin/{entity}' => 'AdminController',
	'jobtitle'       => 'Admin\JobTitleController',
    'spa/{spa}'      => 'SpaController',
]);
