<?php 

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class SpaController extends Controller {

	public function __construct()
	{
		#$this->middleware('auth');
	}

	public function getIndex($spa)
	{
        return view('spa.'.$spa);
	}

}
