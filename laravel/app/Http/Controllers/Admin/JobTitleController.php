<?php 

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Request;
use App\Http\Controllers\Controller;
use App\Entity\Admin\JobTitle;

class JobTitleController extends Controller {

	public function __construct()
	{
		#$this->middleware('auth');
	}

	public function getEdit($id)
	{
        $job = JobTitle::find($id);
        return json_encode($job);
	}

	public function getList()
	{
        $jobs = JobTitle::all();
        return json_encode($jobs);
	}

	public function postAdd()
	{
        $job = new JobTitle;
        $job->job_title = Request::input('job_title');
        $job->save();
        return 'Add';
	}

	public function postUpdate()
	{
        $job = JobTitle::find(Request::input('id'));
        $job->job_title = Request::input('job_title');
        $job->save();
        return 'Update';
	}

	public function postDelete()
	{
        $job = JobTitle::find(Request::input('id'));
        $job->delete();
        return 'Delete';
	}

}
