<!DOCTYPE html>
<html> 
  <head>
    <meta charset="UTF-8">
    <title>ModernHRM (React)</title>
  </head>
  <body> 
    <div id='app'></div>
    <script>var dmt = [];</script>
    <script src="/js/_vendor/spa/react/react.min.js"></script>
    <script src="/js/_vendor/spa/react/ReactRouter.min.js"></script>
    <script src="/js/spa/react/main.js"></script>
    <script src="/js/spa/_entity/admin/jobtitle/array-of-objects.js"></script>
    <script src="/js/spa/react/admin/jobtitle.js"></script>
    <script>app.init();</script>
  </body>
</html>