<!DOCTYPE html>
<html> 
  <head>
    <meta charset="UTF-8">
    <title>ModernHRM (Ember)</title>
  </head>
  <body> 
    @include('spa._ember.main')
    <script>var dmt = [];</script>
    <script src="/js/_vendor/jquery.min.js"></script>
    <script src="/js/_vendor/spa/ember/ember.min.js"></script>
    <script src="/js/_vendor/spa/ember/ember-template-compiler.js"></script>
    <script src="/js/spa/ember/main.js"></script>
    <script src="/js/spa/_entity/admin/jobtitle/array-of-objects.js"></script>
    <script src="/js/spa/ember/admin/jobtitle.js"></script>
    @include('spa._ember.admin.jobtitle')
  </body>
</html>