<!DOCTYPE html>
<html> 
  <head>
    <meta charset="UTF-8">
    <title>ModernHRM (Mithril)</title>
  </head>
  <body> 
    <div id='app'></div>
    <script>var dmt = [];</script>
    <script src="/js/_vendor/spa/mithril/mithril.min.js"></script>
    <script src="/js/spa/mithril/main.js"></script>
    <script src="/js/spa/_entity/admin/jobtitle/array-of-objects.js"></script>
    <script src="/js/spa/mithril/admin/jobtitle.js"></script>
    <script>app.init();</script>
  </body>
</html>