
<script type="text/x-handlebars" data-template-name="jobtitle/add">
  <p>Add
    <form>
      {{input type="text" value=model.job_title}}
      <button {{action 'add'}}>Add</button>
      
      
    </form>
  </p>
</script>
<script type="text/x-handlebars" data-template-name="jobtitle/edit">
  <p>Edit</p>
  <form>
    {{input type="text" value=model.id}}
    {{input type="text" value=model.job_title}}
    <button {{action 'update'}}>Update</button>
    <button {{action 'delete'}}>Delete</button>
    
  </form>
</script>
<script type="text/x-handlebars" data-template-name="jobtitle/list">
  <p>List</p>
  <table class="table">
    <thead>
      <tr>
        <th>ID</th>
        <th>Job Title</th>
      </tr>
    </thead>
    <tbody>{{#each row in list}}
      <tr>
        <th>{{#link-to 'jobtitle.edit' row.id}} {{row.id}} {{/link-to}}</th>
        <th>{{row.job_title}}</th>
      </tr>{{/each}}
    </tbody>
  </table>
</script>