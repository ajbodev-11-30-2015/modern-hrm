<!DOCTYPE html>
<html ng-app='app'> 
  <head>
    <meta charset="UTF-8">
    <title>ModernHRM (Angular)</title>
  </head>
  <body> 
    <div id='app' ui-view></div>
    <script>var dmt = [];</script>
    <script src="/js/_vendor/spa/angular/angular.min.js"></script>
    <script src="/js/_vendor/spa/angular/angular-ui-router.min.js"></script>
    <script src="/js/spa/angular/main.js"></script>
    <script src="/js/spa/_entity/admin/jobtitle/array-of-objects.js"></script>
    <script src="/js/spa/angular/admin/_jobtitle.js"></script>
    <script src="/js/spa/angular/admin/jobtitle.js"></script>
    <script>app.init();</script>
  </body>
</html>