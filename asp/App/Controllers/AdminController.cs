﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace App.Controllers
{
    public class AdminController : Controller
    {
        public ActionResult Entity(string entity)
        {
            return View("~/Views/Admin/" + entity + ".cshtml");
        }
    }
}