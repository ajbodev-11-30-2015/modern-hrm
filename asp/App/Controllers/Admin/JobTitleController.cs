
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using App.DAL;
using App.Models.Admin;

namespace App.Controllers.Admin
{
    public class JobTitleController : Controller
    {
        private AppContext db = new AppContext();

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            JobTitle entity = db.JobTitles.Find(id);
            return Content(JsonConvert.SerializeObject(entity));
        }

        [HttpGet]
        public ActionResult List()
        {
            return Content(JsonConvert.SerializeObject(db.JobTitles));
        }

        [HttpPost]
        public ActionResult Add([Bind(Include = "job_title,job_description")] JobTitle entity)
        {
            db.JobTitles.Add(entity);
            db.SaveChanges();
            return Content("Added");
        }

        [HttpPost]
        public ActionResult Update([Bind(Include = "id,job_title,job_description")] JobTitle entity)
        {
            db.Entry(entity).State = EntityState.Modified;
            db.SaveChanges();
            return Content("Updated");
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            JobTitle entity = db.JobTitles.Find(id);
            db.JobTitles.Remove(entity);
            db.SaveChanges();
            return Content("Deleted");
        }
    }
}