    
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Models.Admin
{
    public class JobTitle
    {
        public int id { get; set; }
        
        public string job_title { get; set; }
        
        public string job_description { get; set; }
        
    }
}