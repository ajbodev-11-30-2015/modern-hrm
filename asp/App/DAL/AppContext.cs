﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

using App.Models.Admin;

namespace App.DAL
{
    public class AppContext : DbContext
    {
        public AppContext() : base("AppContext") { }

        public DbSet<JobTitle> JobTitles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<JobTitle>().ToTable("ohrm_job_title"); 
        }
    }
}