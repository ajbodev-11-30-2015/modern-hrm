
from App import app, render_template

@app.route('/')
def index():
    return 'Hi'

@app.route('/admin/<name>')
def admin(name):
    return render_template('admin/' + name + '.html')