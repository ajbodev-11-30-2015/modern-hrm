
from flask import jsonify
from App import app, db, render_template, request
from App.models.admin.jobtitle import JobTitle

@app.route('/jobtitle/list', methods=['GET'])
def list():
    entities = []
    for entity in JobTitle.query.all():  
        entities.append(entity.toJSON())
    return "[" + ",".join(entities) + "]"

@app.route('/jobtitle/edit/<id>', methods=['GET'])
def edit(id=None):
    entity = JobTitle.query.get(id)
    return entity.toJSON()

@app.route('/jobtitle/add', methods=['POST'])
def add():
    entity = JobTitle()
    entity.job_title = request.form['job_title']
    entity.job_description = request.form['job_description']
    db.session.add(entity)
    db.session.commit()
    return 'Added'

@app.route('/jobtitle/update', methods=['POST'])
def update():
    entity = JobTitle.query.get(request.form['id'])
    entity.job_title = request.form['job_title']
    entity.job_description = request.form['job_description']
    db.session.commit()
    return 'Updated'


@app.route('/jobtitle/delete', methods=['POST'])
def delete():
    entity = JobTitle.query.get(request.form['id'])
    db.session.delete(entity)
    db.session.commit()
    return request.form['id']