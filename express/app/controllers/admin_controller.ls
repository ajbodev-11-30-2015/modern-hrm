
o = {}

o.route = 'admin'

o.get = 
  ':entity': (req, res) !->
    entity = req.params.entity
    res.render 'admin/' + entity 

if typeof module == 'object' then module.exports = o