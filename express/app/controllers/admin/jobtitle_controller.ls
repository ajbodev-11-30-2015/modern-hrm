
JobTitle = use 'models/admin/jobtitle'

o = {}

o.route = 'jobtitle'

o.get = 
  'list': (req, res) !->
    JobTitle.all (err, entities) !-> 
      res.send entities
  'edit/:id': (req, res) !->
    JobTitle.findById req.params.id, (err, entity) !->
      res.send entity
    
o.post = 
  'add': (req, res) !->
    entity = new JobTitle!
    entity.job_title  = req.body.job_title
    entity.save!
    res.send 'Added ' + req.body.job_title
  'update': (req, res) !->
    JobTitle.findById req.body.id, (err, entity) !->
      entity.job_title = req.body.job_title;
      entity.save!
    res.send 'Updated ' + req.body.id + ':' + req.body.job_title
  'delete': (req, res) !->
    JobTitle.destroyById req.body.id, (err) !->
    res.send 'Deleted ' + req.body.id

if typeof module == 'object' then module.exports = o