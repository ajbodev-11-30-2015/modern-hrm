
express  = require 'express'

o = {}

o.router = express.Router!
 
o.init = !->
  o.generateAll o.controllers

o.controllers = 
  'admin'
  'admin/jobtitle'
  ...

o.generateAll = (controllers) !->
  for let controller in controllers
    o.generate (use 'controllers/' + controller + '_controller')
      
o.generate = (controller) !->
  _generate = (http) !->
    for let route, method of controller[http]
      o.router[http] '/' + controller.route + '/' + route, method
  _generate 'get'
  _generate 'post'
  
if typeof module == 'object' then module.exports = o