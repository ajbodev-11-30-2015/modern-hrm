
express     = require 'express'
jade        = require 'jade'
bodyParser  = require 'body-parser'

global.use  = require './use.ls'
 
(use 'db').init!

routes      = use 'routes'
routes.init!

app         = express!

app.set     'views',       'views'
app.set     'view engine', 'jade'
 
app.use     bodyParser.urlencoded extended: false
app.use     '/',        routes.router
app.use     '/public',  express.static 'public'

app.listen 11004, !->
  console.log 'App on port 11004'